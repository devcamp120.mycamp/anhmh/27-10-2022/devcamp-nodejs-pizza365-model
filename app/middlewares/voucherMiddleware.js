const { response } = require("express");

const printVoucherMiddleware = (request, response, next) => {
    console.log("Course URL: " + request.url);
    console.log(response);
    next();
}

module.exports = {
    printVoucherMiddleware: printVoucherMiddleware
}